import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:knoworld/utils/constants.dart';
import 'package:knoworld/viewmodels/new_article_list_view_model.dart';
import 'package:knoworld/widgets/news_grid.dart';
import 'package:provider/provider.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({Key? key}) : super(key: key);

  @override
  State<NewsScreen> createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  @override
  void initState() {
    Provider.of<NewsArticleListViewModel>(context, listen: false)
        .topHeadLines();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var listViewModel = Provider.of<NewsArticleListViewModel>(context);
    return Scaffold(
        appBar: AppBar(
          actions: [
            PopupMenuButton(
                icon: const Icon(Icons.more_vert),
                onSelected: (country){
                  listViewModel.topCountryHeadLines(Constants.countries[country]!);
                },
                itemBuilder: (BuildContext context) {
                  return Constants.countries.keys
                      .map<PopupMenuEntry>(
                        (e) => PopupMenuItem<String>(
                          value: e,
                          child: Text(e),
                        ),
                      )
                      .toList();
                })
          ],
        ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Padding(
                padding: EdgeInsets.only(left: 30),
                child: Text(
                  'KnoWorld',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Divider(
                color: Color(0xffff8a30),
                height: 40,
                thickness: 8,
                indent: 30,
                endIndent: 20,
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: 30,
                  top: 15,
                  bottom: 15,
                ),
                child: Text(
                  'HeadLine',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                child: NewsGrid(),
              ),
            ],
          ),
        ));
  }
}
