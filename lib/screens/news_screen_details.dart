import 'package:flutter/material.dart';
import 'package:knoworld/viewmodels/news_article_view_model.dart';
import 'package:knoworld/widgets/circle_image.dart';

class NewsScreenDetails extends StatelessWidget {
  final NewsArticleViewModel? model;

  const NewsScreenDetails({Key? key,this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          children: [
            const CircleAvatar(
              backgroundImage: AssetImage(
                'assets/images/profile.jpeg',
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Author',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 12,
                        fontWeight: FontWeight.bold),
                  ),
                  Container(
                    width: 200,
                    child: Text(
                      model?.author ?? ' ',
                      style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 30, right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                alignment: Alignment.centerLeft,
                children: const [
                  Divider(
                    color: Color(0xffff8a30),
                    height: 80,
                    thickness: 20,
                  ),
                  Text(
                    ' Headline',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
              Text(
                model?.title ?? 'undefined',
                style: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  wordSpacing: 3,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                model?.publishedAt ?? 'noDate',
                style: const TextStyle(fontSize: 16, color: Colors.grey),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                height: 200,
                child: CircleImage(
                  imageUrl: model?.imageUrl ?? ' noImage',
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                model?.descripton ?? 'noDescripton',
                style: const TextStyle(
                  fontSize: 16,
                  wordSpacing: 3,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
