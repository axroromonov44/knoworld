import 'package:flutter/material.dart';
import 'package:knoworld/screens/news_screen_details.dart';
import 'package:knoworld/viewmodels/new_article_list_view_model.dart';
import 'package:knoworld/widgets/circle_image.dart';
import 'package:provider/provider.dart';

class NewsGrid extends StatefulWidget {
  const NewsGrid({Key? key}) : super(key: key);

  @override
  State<NewsGrid> createState() => _NewsGridState();
}

class _NewsGridState extends State<NewsGrid> {
  void _showNewsScreenDetails(context, model) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) {
          return NewsScreenDetails(model: model);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var listViewModels = Provider.of<NewsArticleListViewModel>(context);

    return GridView.builder(
      physics: BouncingScrollPhysics(),
      gridDelegate:
          const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemCount: listViewModels.articles.length,
      itemBuilder: (_, index) {
        var listViewModel = listViewModels.articles[index];
        return GestureDetector(
          onTap: () {
            _showNewsScreenDetails(context, listViewModel);
          },
          child: GridTile(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              child: CircleImage(
                imageUrl: listViewModel.imageUrl ?? '',
              ),
            ),
            footer: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
              ),
              child: Text(
                listViewModel.title!,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        );
      },
    );
  }
}
