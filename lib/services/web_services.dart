import 'package:dio/dio.dart';
import 'package:knoworld/models/news_article.dart';
import 'package:knoworld/utils/constants.dart';

class WebService {
  var dio = Dio();
  Future<List<Article>> fetchHeadlinesByCountry(String country) async {
    final response = await dio.get(Constants.headlinesFor(country));

    if (response.statusCode == 200) {
      final result = response.data;
      Iterable list = result['articles'];
      return list.map((article) => Article.fromJson(article)).toList();
    } else {
      throw Exception("Failled to get top news");
    }
  }

  Future<List<Article>?> fetchTopHeadLines() async {

    final response = await dio.get(Constants.TOP_HEADLINES_URL);
    if (response.statusCode == 200) {
      final result = response.data;
      Iterable list = result['articles'];
      return list.map((e) => Article.fromJson(e)).toList();
    } else {
      throw Exception("Failed to get top news");
    }
  }
}
