import 'package:flutter/material.dart';
import 'package:knoworld/models/news_article.dart';
import 'package:knoworld/services/web_services.dart';
import 'package:knoworld/viewmodels/news_article_view_model.dart';

enum LoadingStatus {
  completed,
  searching,
  empty,
}

class NewsArticleListViewModel with ChangeNotifier {
  LoadingStatus loadingStatus = LoadingStatus.empty;
  List<NewsArticleViewModel> articles = [];

  void topCountryHeadLines(String country) async {
    List<Article>? newsArticles =
    (await WebService().fetchHeadlinesByCountry(country));
    loadingStatus = LoadingStatus.searching;
    notifyListeners();

    articles =
        newsArticles.map((e) => NewsArticleViewModel(article: e)).toList();

    if (articles.isEmpty) {
      loadingStatus = LoadingStatus.empty;
    } else {
      loadingStatus = LoadingStatus.completed;
    }
  }

  void topHeadLines() async {
    List<Article>? newsArticles = await WebService().fetchTopHeadLines();
    loadingStatus = LoadingStatus.searching;
    notifyListeners();

    articles =
        newsArticles!.map((e) => NewsArticleViewModel(article: e)).toList();

    if (articles.isEmpty) {
      loadingStatus = LoadingStatus.empty;
    } else {
      loadingStatus = LoadingStatus.completed;
    }
  }
}
