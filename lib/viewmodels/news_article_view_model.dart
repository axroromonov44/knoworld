import 'package:intl/intl.dart';
import 'package:knoworld/models/news_article.dart';

class NewsArticleViewModel {
  Article _article;

  NewsArticleViewModel({required Article article}) : _article = article;

  String? get title {
    return _article.title;
  }

  String? get descripton {
    return _article.description;
  }

  String? get imageUrl {
    return _article.urlToImage;
  }

  String? get url {
    return _article.url;
  }

  String? get author {
    return _article.author;
  }

  String? get publishedAt {
    final dateTime = DateFormat('yyyy-mm-ddTHH:mm:ssZ').parse(
      _article.publishedAt!.toIso8601String(),
      true,
    );
    return DateFormat.yMMMMEEEEd('en-us').format(dateTime).toString();
  }
}
