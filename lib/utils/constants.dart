class Constants {
  static const API_KEY = '821fadeb6e0e40599c480c84e461782d';
  static const String TOP_HEADLINES_URL =
      'https://newsapi.org/v2/top-headlines?country=us&apiKey=$API_KEY';

  static String headlinesFor(String country) {
    return 'https://newsapi.org/v2/top-headlines?country=$country&apiKey=$API_KEY';
  }

  static const Map<String, String> countries = {
    "USA": "us",
    "India": "in",
    "Korea": "kr",
    "China": "ch"
  };
}
